import React from 'react';
import classNames from 'classnames';

import './index.styl';

export default function Page(props) {
	return <div className={classNames('_Page', props.className)}>
		{props.title
		&& <h1>{props.title}</h1>}
		{props.children}
	</div>
}