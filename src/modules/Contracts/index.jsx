import React from 'react'
import {connect} from 'react-redux'

import Page from 'components/Page'

import './index.styl'

@connect(state => ({
	contracts: state.contracts
}))
export default class Contracts extends React.Component {
	renderContract = (contract) => {
		return <div className="__Contracts__Contract" key={contract.id}>
			<h2>Contract #{contract.id}</h2>
			<p>{contract.name}</p>
		</div>
	}
	render() {
		return <Page className="__Contracts" title="Contracts">
			<div className="__Contracts__List">
				{this.props.contracts.map(this.renderContract)}
			</div>
		</Page>
	}
}