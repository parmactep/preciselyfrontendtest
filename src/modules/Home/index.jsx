import React from 'react';
import {Link} from 'react-router-dom';

import Page from 'components/Page';

import './index.styl';

export default class Home extends React.Component {
	render() {
		return <Page className="__Home">
			<h1>The Contract Automation Platform</h1>
			<div className="__Home__Links">
				<Link to="/contracts">Contracts</Link>
				<Link to="/customers">Customers</Link>
			</div>
		</Page>
	}
}