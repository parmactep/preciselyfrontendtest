import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import {getCustomersWithContracts} from 'store/reducer'
import * as actions from 'store/customers/actions'

import Page from 'components/Page'

import './index.styl'

@connect(
	state => ({
		customers: getCustomersWithContracts(state)
	}),
	dispatch => bindActionCreators(actions, dispatch)
)
export default class Customers extends React.Component {
	handleRemove = (e) => {
		this.props.removeCustomer(e.target.dataset.id)
	}
	renderContract = (contract) => {
		return <div className="__Customers__Contract" key={contract.id}>
			Contract #{contract.id}
		</div>
	}
	renderCustomer = (customer) => {
		return <div className="__Customers__Customer" key={customer.id}>
			<h2>{customer.name}</h2>
			<button onClick={this.handleRemove} data-id={customer.id}>Delete</button>
			{customer.contracts.map(this.renderContract)}
		</div>
	}
	render() {
		return <Page className="__Customers" title="Customers">
			<div className="__Customers__List">
				{this.props.customers.map(this.renderCustomer)}
			</div>
		</Page>
	}
}