import {range} from 'lodash';

export const initialState = range(0, 26).map(contract => ({
	id: contract + '',
	name: 'Lorem ipsum',
	customerId: contract & 1 && '2' || '1'
}))

export default function (state = initialState, action) {
	switch (action.type) {
		case 'REMOVE_CUSTOMER':
			return state.filter(contract => contract.customerId !== action.id)
		default:
			return state
	}
}