import {combineReducers} from 'redux'

import contracts from './contracts/reducer'
import customers from './customers/reducer'

export default combineReducers({
	contracts,
	customers
})

export function getCustomersWithContracts(state) {
	return state.customers.map(customer => ({
		...customer,
		contracts: state.contracts.filter(contract => contract.customerId === customer.id)
	}))
}