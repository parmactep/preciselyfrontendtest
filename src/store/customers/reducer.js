const initialState = [
	{
		id: '1',
		name: 'Lawyers Ltd.'
	},
	{
		id: '2',
		name: 'Legal Co.'
	}
]

export default function (state = initialState, action) {
	switch (action.type) {
		case 'REMOVE_CUSTOMER':
			return state.filter(customer => customer.id !== action.id)
		default:
			return state
	}
}