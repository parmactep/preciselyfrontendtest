import React from 'react';
import {render} from 'react-dom';
import {createStore} from 'redux'
import {Provider} from 'react-redux';


import App from './App';
import reducer from 'store/reducer';

const store = createStore(reducer);

import './index.styl';

render(
	<Provider store={store}>
		<App/>
	</Provider>,
	document.getElementById('app')
);

//@TODO: Should use flow or TypeScript if app will grow :)