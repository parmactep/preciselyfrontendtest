import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import Home from 'modules/Home';
import Contracts from 'modules/Contracts';
import Customers from 'modules/Customers';

export default class App extends React.Component {
	render() {
		return <BrowserRouter>
			<Switch>
				<Route exact path="/" component={Home}/>
				<Route path="/contracts" component={Contracts}/>
				<Route path="/customers" component={Customers}/>
			</Switch>
		</BrowserRouter>
	}
}